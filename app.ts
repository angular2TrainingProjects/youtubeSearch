import { 
    Component,
    NgModule,
    Inject,
    Injectable,
    OnInit,
    ElementRef,
    EventEmitter
} from "@angular/core";//
import {Http,Response} from "@angular/http";//
import {HttpModule} from "@angular/http";//
import {Observable} from "rxjs/Rx";   //  
import { BrowserModule } from "@angular/platform-browser";//
import { platformBrowserDynamic } from "@angular/platform-browser-dynamic";//

class SearchResult {
     id: string;
     title: string;
     description: string;
     thumbnail: string;
     videoUrl: string;
     constructor(obj ?: any) {
     this.id = obj && obj.id || null;
     this.title=obj && obj.title || null;
     this.description=obj && obj.description || null;
     this.thumbnail=obj && obj.thumbnail || null;
     this.videoUrl=obj && obj.videoUrl || `https://www.youtube.com/watch?v=${this.id}`;
     }
}

export var YouTube_API_KEY: string = 'AIzaSyCzZx3GWVORS0sOpzeJF9MxbwX_eLQbUCw';
export var YouTube_API_URL: string = 'https://www.googleapis.com/youtube/v3/search';     

@Injectable()
export class YoutubeService {
     constructor(private http: Http ,
          @Inject(YouTube_API_KEY) private apikey: string,
          @Inject(YouTube_API_URL) private apiurl: string) {
     }
     search(query: string): Observable<SearchResult[]> {
          let params: string = [
          `q=${query}`,
          `key=${this.apikey}`,
          `part=snippet`,
          `type=video`,
          `maxResults=10`
          ].join('&');
          let queryUrl: string = `${this.apiurl}?${params}`;
          return this.http.get(queryUrl)
          .map((res : Response) => {
               return res.json().items.map(item => {
                    return new SearchResult({
                         id: item.id.videoId,
                         title: item.snippet.title,
                         description: item.snippet.description,
                         thumbnailUrl: item.snippet.thumbnails.high.url
                    });
               });
          });
     }
}


export var youTubeServiceInjectables: Array<any> = [
  {provide: YoutubeService, useClass: YoutubeService},
  {provide: YouTube_API_KEY, useValue: YouTube_API_KEY},
  {provide: YouTube_API_URL, useValue: YouTube_API_URL}
];


@Component({
     selector: "search-box",
     outputs: ['loading','results'],
     template: `
          <input type="text" placeholder="search" autofocus class="form-control" >
          `
})
class SearchBox implements OnInit {
     loading: EventEmitter< boolean > = new EventEmitter< boolean >();
     results: EventEmitter<SearchResult[]> = new EventEmitter<SearchResult[]>();
     constructor(private youtube: YoutubeService, private el: ElementRef) { }
     ngOnInit():void {
          // let t=Observable.fromEvent(this.el.nativeElement, 'keyup')
          // .subscribe((el: any) => {
          //      let inputValue: Array<string> = new Array(el.target.value);
          //      console.log(inputValue.filter((text: string) => text.length > 1));
          //      return el.target.vaule;});

         let t=Observable.fromEvent(this.el.nativeElement, 'keyup')
          .map((e: any) => e.target.value)
          .filter((text: string) => text.length > 1)
          .debounceTime(3100)
          .do(() => this.loading.next(true))
          .map((query: string) => this.youtube.search(query))
          .switch()
          .subscribe(
          (results: SearchResult[]) => {
               this.loading.next(false);
               this.results.next(results);
          },
          (err: any) => {
               console.log(err);
               this.loading.next(false);
          },
          () => {
               this.loading.next(false);
          }
          );

     }
}
@Component({
     inputs: ['result'],
     selector: 'search-result',
     template: `
          <div class="col-sm-6 col-md-3">
          <div class="thumbnail">
          <img src="{{result.thumbnailUrl}}">
          <div class="caption">
          <h3>{{result.title}}</h3>
          <p>{{result.description}}</p>
          <p><a href="{{result.videoUrl}}"
          class="btn btn-primary" role="button">Watch</a></p>
          </div>
          </div>
          </div>
          `
})
class SearchResultCom{
      result: SearchResult;
}
@Component({
     selector: 'youtube-search',
     template: `
     <div class='container'>
     <div class="page-header">
     <h1>YouTube Search
     <div
     style="float: right;" class="btn btn-primary"
     *ngIf="loading" src="">loading</div>
     </h1>
     </div>
     <div class="row">
     <div class="input-group input-group-lg col-md-12">
     <search-box
     (loading)="loading = $event"
     (results)="updateResults($event)"
     ></search-box>
     </div>
     </div>
     <div class="row">
     <search-result
     *ngFor="let result of results"
     [result]="result">
     </search-result>
     </div>
     </div>
     `
})
export class YouTubeSearchComponent {
     results: SearchResult[];
     constructor() {}
     updateResults(results: SearchResult[]): void {
          this.results = results;
          // console.log("results:", this.results); // uncomment to take a look
     }
}


@NgModule({
	declarations: [ SearchResultCom,  SearchBox, YouTubeSearchComponent  ] , 
	imports: [ BrowserModule,HttpModule ] ,
                 providers: [ youTubeServiceInjectables ] ,
	bootstrap: [ YouTubeSearchComponent ]
})
class YouTubeSearchModule {}
platformBrowserDynamic().bootstrapModule(YouTubeSearchModule);
