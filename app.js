"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
const core_1 = require("@angular/core"); //
const http_1 = require("@angular/http"); //
const http_2 = require("@angular/http"); //
const Rx_1 = require("rxjs/Rx"); //  
const platform_browser_1 = require("@angular/platform-browser"); //
const platform_browser_dynamic_1 = require("@angular/platform-browser-dynamic"); //
class SearchResult {
    constructor(obj) {
        this.id = obj && obj.id || null;
        this.title = obj && obj.title || null;
        this.description = obj && obj.description || null;
        this.thumbnail = obj && obj.thumbnail || null;
        this.videoUrl = obj && obj.videoUrl || `https://www.youtube.com/watch?v=${this.id}`;
    }
}
exports.YouTube_API_KEY = 'AIzaSyCzZx3GWVORS0sOpzeJF9MxbwX_eLQbUCw';
exports.YouTube_API_URL = 'https://www.googleapis.com/youtube/v3/search';
let YoutubeService = class YoutubeService {
    constructor(http, apikey, apiurl) {
        this.http = http;
        this.apikey = apikey;
        this.apiurl = apiurl;
    }
    search(query) {
        let params = [
            `q=${query}`,
            `key=${this.apikey}`,
            `part=snippet`,
            `type=video`,
            `maxResults=10`
        ].join('&');
        let queryUrl = `${this.apiurl}?${params}`;
        return this.http.get(queryUrl)
            .map((res) => {
            return res.json().items.map(item => {
                return new SearchResult({
                    id: item.id.videoId,
                    title: item.snippet.title,
                    description: item.snippet.description,
                    thumbnailUrl: item.snippet.thumbnails.high.url
                });
            });
        });
    }
};
YoutubeService = __decorate([
    core_1.Injectable(),
    __param(1, core_1.Inject(exports.YouTube_API_KEY)),
    __param(2, core_1.Inject(exports.YouTube_API_URL)), 
    __metadata('design:paramtypes', [http_1.Http, String, String])
], YoutubeService);
exports.YoutubeService = YoutubeService;
exports.youTubeServiceInjectables = [
    { provide: YoutubeService, useClass: YoutubeService },
    { provide: exports.YouTube_API_KEY, useValue: exports.YouTube_API_KEY },
    { provide: exports.YouTube_API_URL, useValue: exports.YouTube_API_URL }
];
let SearchBox = class SearchBox {
    constructor(youtube, el) {
        this.youtube = youtube;
        this.el = el;
        this.loading = new core_1.EventEmitter();
        this.results = new core_1.EventEmitter();
    }
    ngOnInit() {
        // let t=Observable.fromEvent(this.el.nativeElement, 'keyup')
        // .subscribe((el: any) => {
        //      let inputValue: Array<string> = new Array(el.target.value);
        //      console.log(inputValue.filter((text: string) => text.length > 1));
        //      return el.target.vaule;});
        let t = Rx_1.Observable.fromEvent(this.el.nativeElement, 'keyup')
            .map((e) => e.target.value)
            .filter((text) => text.length > 1)
            .debounceTime(3100)
            .do(() => this.loading.next(true))
            .map((query) => this.youtube.search(query))
            .switch()
            .subscribe((results) => {
            this.loading.next(false);
            this.results.next(results);
        }, (err) => {
            console.log(err);
            this.loading.next(false);
        }, () => {
            this.loading.next(false);
        });
    }
};
SearchBox = __decorate([
    core_1.Component({
        selector: "search-box",
        outputs: ['loading', 'results'],
        template: `
          <input type="text" placeholder="search" autofocus class="form-control" >
          `
    }), 
    __metadata('design:paramtypes', [YoutubeService, core_1.ElementRef])
], SearchBox);
let SearchResultCom = class SearchResultCom {
};
SearchResultCom = __decorate([
    core_1.Component({
        inputs: ['result'],
        selector: 'search-result',
        template: `
          <div class="col-sm-6 col-md-3">
          <div class="thumbnail">
          <img src="{{result.thumbnailUrl}}">
          <div class="caption">
          <h3>{{result.title}}</h3>
          <p>{{result.description}}</p>
          <p><a href="{{result.videoUrl}}"
          class="btn btn-primary" role="button">Watch</a></p>
          </div>
          </div>
          </div>
          `
    }), 
    __metadata('design:paramtypes', [])
], SearchResultCom);
let YouTubeSearchComponent = class YouTubeSearchComponent {
    constructor() {
    }
    updateResults(results) {
        this.results = results;
        // console.log("results:", this.results); // uncomment to take a look
    }
};
YouTubeSearchComponent = __decorate([
    core_1.Component({
        selector: 'youtube-search',
        template: `
     <div class='container'>
     <div class="page-header">
     <h1>YouTube Search
     <div
     style="float: right;" class="btn btn-primary"
     *ngIf="loading" src="">loading</div>
     </h1>
     </div>
     <div class="row">
     <div class="input-group input-group-lg col-md-12">
     <search-box
     (loading)="loading = $event"
     (results)="updateResults($event)"
     ></search-box>
     </div>
     </div>
     <div class="row">
     <search-result
     *ngFor="let result of results"
     [result]="result">
     </search-result>
     </div>
     </div>
     `
    }), 
    __metadata('design:paramtypes', [])
], YouTubeSearchComponent);
exports.YouTubeSearchComponent = YouTubeSearchComponent;
let YouTubeSearchModule = class YouTubeSearchModule {
};
YouTubeSearchModule = __decorate([
    core_1.NgModule({
        declarations: [SearchResultCom, SearchBox, YouTubeSearchComponent],
        imports: [platform_browser_1.BrowserModule, http_2.HttpModule],
        providers: [exports.youTubeServiceInjectables],
        bootstrap: [YouTubeSearchComponent]
    }), 
    __metadata('design:paramtypes', [])
], YouTubeSearchModule);
platform_browser_dynamic_1.platformBrowserDynamic().bootstrapModule(YouTubeSearchModule);
//# sourceMappingURL=app.js.map